import { APIPerson } from './api/person.api';
import { MainService } from './services/mainServ';
import { Component, ViewChild } from '@angular/core';
import { Person } from './interfaces/person.interface';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

let ELEMENT_DATA: Person[] = [
  // {name: "Nolberto Mejia Pineda", age: 30, gender: "HOMBRE", file: "Archivo.txt"},
  // {name: "Diana Laura Beltran Baldenebro", age: 24, gender: "MUJER", file: "Archivo1.txt"},
  // {name: "Ana Sofía Mejia Beltran", age: 2, gender: "MUJER", file: "Archivo2.txt"}
]

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'PERSONAS';
  displayedColumns: string[] = ['name', 'age', 'gender', 'file'];
  dataSource = new MatTableDataSource<Person>(ELEMENT_DATA);

  public personForm: FormGroup;

  @ViewChild(MatPaginator)
  paginator!: MatPaginator | null;
  @ViewChild(MatSort)
  sort!: MatSort;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort;
  }

  constructor(private form: FormBuilder, public mainServ: MainService,
    private apiService: APIPerson, private snackBar: MatSnackBar,){
    this.personForm = form.group({
      "name": ["", [Validators.required, this.mainServ.noWhitespaceValidator]],
      "file": ["", [Validators.required]]
    });

    if(localStorage.getItem("dataSource")){
      ELEMENT_DATA = JSON.parse(localStorage.getItem('dataSource') || "[]");
    };

    this.dataSource = new MatTableDataSource<Person>(ELEMENT_DATA);
  }

  getErrorMessage(control: string) {
    return this.personForm.controls[control].hasError('required') ? 'Este campo es necesario.' : '';
  }

  onSave(){
    debugger
    if (this.personForm.valid) {
      return this.apiService.getPerson().subscribe((data: Person[]) => {
        debugger
        let name:string = this.personForm.controls["name"].value;
        let _file:File = this.personForm.controls["file"].value;
        let fileName: string = _file.name;
        let person: Person = {
          name:name,
          age: data[0].age,
          gender: data[0].gender,
          file: fileName,
          timeStamp: new Date()
        };

        if(localStorage.getItem("dataSource")){
          ELEMENT_DATA = JSON.parse(localStorage.getItem('dataSource') || "[]");
        }

        ELEMENT_DATA.push(person);
        localStorage.setItem('dataSource', JSON.stringify(ELEMENT_DATA.sort()));
        this.dataSource = new MatTableDataSource<Person>(ELEMENT_DATA);
        this.dataSource.paginator = this.paginator
        this.dataSource.sort = this.sort;
        this.personForm.reset();
        this.personForm.controls["name"].setErrors(null);
        this.personForm.controls["name"].clearValidators();
        this.personForm.controls["file"].setErrors(null);
        this.personForm.controls["file"].clearValidators();
        this.snackBar.open('Persona guardad con exito!', 'OK', { duration: 2400 });
        debugger
      });
    }
    return;
  }
}
