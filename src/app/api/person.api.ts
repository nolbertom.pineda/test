import { Person } from '../interfaces/person.interface';
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { retry, catchError, map, take } from 'rxjs/operators';
import { PersonAPI } from '../interfaces/person.api.interface';

@Injectable({
  providedIn: "root"
})
export class APIPerson {
  private APIURL = "https://randomapi.com/api/ezt889e9?key=J500-BGVD-BEGD-NTHI&fmt=json&results=1&noinfo";

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  getPerson(): Observable<Person[]> {
    return this.http.get<PersonAPI>(this.APIURL)
    .pipe(
      map(res => {
        debugger
        return res.results;
      })
    );
  }

  // getRoles() {
  //   this.datachange = new BehaviorSubject<Rol[]>([]);
  //   this.rolesLength = 0;
  //   this.isLoadingRoles = true;

  //   this.http.get<Rol[]>(`${this.url}/rolesAPI/roles`, { withCredentials: true }).
  //     pipe(
  //       take(1),
  //       map(res => {
  //         this.isLoadingRoles = false;
  //         this.rolesLength = res.length;
  //         return res;
  //       })
  //     ).subscribe((roles: Rol[]) => {
  //       roles.forEach((rol) => {
  //         const copiedData: Rol[] = this.data.slice();
  //         copiedData.push(rol);
  //         this.datachange.next(copiedData);
  //       });
  //     });
  // }

  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
}
