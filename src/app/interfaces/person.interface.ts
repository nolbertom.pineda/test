export interface Person {
  name: string,
  age: number,
  gender: string,
  file: string,
  timeStamp: Date
}
