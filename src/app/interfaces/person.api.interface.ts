import { Person } from "./person.interface";

export interface PersonAPI {
  results: Person[]
}
