import { Injectable } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  public breakPoint: boolean = false;

  constructor(_bpo: BreakpointObserver) {

    _bpo.observe('(min-width: 900px)').subscribe((bp: any) => {
      this.breakPoint = bp.matches
    });
  }

  noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;

    return isValid ? null : { 'required': true };
  }
}
